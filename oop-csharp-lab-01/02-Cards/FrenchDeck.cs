﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards
{
    class FrenchDeck
    {
        private Card[] cards;

        public FrenchDeck()
        {
            cards = new Card[Enum.GetNames(typeof(FrenchSeed)).Length * Enum.GetNames(typeof(FrenchValue)).Length + Enum.GetNames(typeof(Jolly)).Length]; // 4 * 13 + 2 = 54
        }
        public Card this[FrenchSeed s, FrenchValue v]
        {
            get
            {
                foreach (Card c in cards)
                {
                    if (c.Value == v.ToString() && c.Seed == s.ToString())
                    {
                        return c;
                    }

                }
                return null;
            }

        }

        public Card this[Jolly v]
        {
            get
            {
                foreach (Card c in cards)
                {
                    if (c.Value == v.ToString() )
                    {
                        return c;
                    }

                }
                return null;
            }

        }

        public void Initialize()
        {

            int numSeed = Enum.GetNames(typeof(FrenchSeed)).Length;
            int numVal = Enum.GetNames(typeof(FrenchValue)).Length;
            FrenchSeed[] elencoSeed = (FrenchSeed[])Enum.GetValues(typeof(FrenchSeed));
            FrenchValue[] elencoValue = (FrenchValue[])Enum.GetValues(typeof(FrenchValue));
            int i = 0;

            foreach (FrenchSeed seed in elencoSeed)
            {
                foreach (FrenchValue value in elencoValue)
                {
                    cards[i] = new Card(value.ToString(), seed.ToString());
                    i++;
                }
            }

            foreach (Jolly j in (Jolly[])Enum.GetValues(typeof(Jolly)))
            {
                cards[i] = new Card("JOLLY", j.ToString());
                i++;
            }
        }

        public void Print()
        {
            foreach (Card i in cards)
            {
                Console.WriteLine(i);
            }
        }

    }

    enum FrenchSeed
    {
        PICCHE, // 0
        FIORI, // 1 di default 
        QUADRI,
        CUORI
    }

    enum FrenchValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        OTTO,
        NOVE,
        DIECI,
        J,
        Q,
        K
    }

    enum Jolly
    {
        NERO,
        ROSSO
    }
}


