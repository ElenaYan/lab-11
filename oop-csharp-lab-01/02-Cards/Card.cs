﻿namespace Cards
{
    class Card
    {
        public string Seed { get; set; }
        public string Value { get; set; }

        public Card(string value, string seed)
        {
            this.Seed = seed;
            this.Value = value;
        }

        public override string ToString()
        {
            // TODO comprendere il meccanismo denominato in C# "string interpolation"
            return $"{this.GetType().Name}(Name={this.Value}, Seed={this.Seed})";
        }
    }

    
}
