﻿using System;

namespace Cards
{
    class Deck
    {
        private Card[] cards;

        public Deck()
        {
            cards = new Card[Enum.GetNames(typeof(ItalianSeed)).Length * Enum.GetNames(typeof(ItalianValue)).Length]; // 4 * 10 = 40
        }
        public Card this[ItalianValue v, ItalianSeed s]
        {
            get
            {
                foreach (Card c in cards)
                {
                    if (c.Value == v.ToString() && c.Seed == s.ToString())
                    {
                        return c;
                    }

                } 
                return null;
            }
            
        }

            public void Initialize()
        {
            /*
             * == README ==
             * 
             * I due enumerati (ItalianSeed e ItalianValue) definiti in fondo a questo file rappresentano rispettivamente semi e valori
             * di un tipo mazzo di carte da gioco italiano.
             * 
             * Questo metodo deve inizializzare il mazzo (l'array cards) considerando tutte le combinazioni possibili.
             * 
             * Nota - Dato un generico enumerato MyEnum:
             *   a) è possibile ottenere il numero dei valori presenti con Enum.GetNames(typeof(MyEnum)).Length
             *   b) è possibile ottenere l'elenco dei valori presenti con (MyEnum[])Enum.GetValues(typeof(MyEnum))
             * 
             */

            int numSeed = Enum.GetNames(typeof(ItalianSeed)).Length;
            int numVal = Enum.GetNames(typeof(ItalianValue)).Length;
            ItalianSeed[]  elencoSeed =  (ItalianSeed[])Enum.GetValues(typeof(ItalianSeed)) ;
            ItalianValue[] elencoValue = (ItalianValue[])Enum.GetValues(typeof(ItalianValue));
            int i=0;

            foreach (ItalianSeed seed in elencoSeed)
            {
                foreach (ItalianValue value in elencoValue)
                {
                    cards[i] = new Card(value.ToString(), seed.ToString());
                    i++;
                }
            }
      
        }

        public void Print()
        {
            foreach (Card i in cards)
            {
                Console.WriteLine(i);
            }
        }

    }


    enum ItalianSeed
    {
        DENARI, // 0
        COPPE, // 1 di default 
        SPADE,
        BASTONI
    }

    enum ItalianValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        FANTE,
        CAVALLO,
        RE
    }
}
